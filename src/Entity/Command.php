<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommandRepository")
 */
class Command
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="commands")
     */
    private $User;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Cart", inversedBy="command", cascade={"persist", "remove"})
     */
    private $Cart;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $Date;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CartLine", mappedBy="command")
     */
    private $CartLine;

    public function __construct()
    {
        $this->CartLine = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getCart(): ?Cart
    {
        return $this->Cart;
    }

    public function setCart(?Cart $Cart): self
    {
        $this->Cart = $Cart;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(?\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    /**
     * @return Collection|CartLine[]
     */
    public function getCartLine(): Collection
    {
        return $this->CartLine;
    }

    public function addCartLine(CartLine $cartLine): self
    {
        if (!$this->CartLine->contains($cartLine)) {
            $this->CartLine[] = $cartLine;
            $cartLine->setCommand($this);
        }

        return $this;
    }

    public function removeCartLine(CartLine $cartLine): self
    {
        if ($this->CartLine->contains($cartLine)) {
            $this->CartLine->removeElement($cartLine);
            // set the owning side to null (unless already changed)
            if ($cartLine->getCommand() === $this) {
                $cartLine->setCommand(null);
            }
        }

        return $this;
    }
}
