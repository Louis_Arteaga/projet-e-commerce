<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartRepository")
 */
class Cart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CartLine", mappedBy="cart")
     */
    private $cartLines;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="cart", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Command", mappedBy="Cart", cascade={"persist", "remove"})
     */
    private $command;

    public function __construct()
    {
        $this->cartLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|CartLine[]
     */
    public function getCartLines(): Collection
    {
        return $this->cartLines;
    }

    public function addCartLine(CartLine $cartLine): self
    {
        if (!$this->cartLines->contains($cartLine)) {
            $this->cartLines[] = $cartLine;
            $cartLine->setCart($this);
        }

        return $this;
    }

    public function removeCartLine(CartLine $cartLine): self
    {
        if ($this->cartLines->contains($cartLine)) {
            $this->cartLines->removeElement($cartLine);
            // set the owning side to null (unless already changed)
            if ($cartLine->getCart() === $this) {
                $cartLine->setCart(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        // set (or unset) the owning side of the relation if necessary
        $newCart = $user === null ? null : $this;
        if ($newCart !== $user->getCart()) {
            $user->setCart($newCart);
        }

        return $this;
    }

    public function getCommand(): ?Command
    {
        return $this->command;
    }

    public function setCommand(?Command $command): self
    {
        $this->command = $command;

        // set (or unset) the owning side of the relation if necessary
        $newCart = $command === null ? null : $this;
        if ($newCart !== $command->getCart()) {
            $command->setCart($newCart);
        }

        return $this;
    }

    public function getCount() {
        //créer variable à zéero
        $count = 0;
        //faire un foreach sur this cartLines
        foreach ($this->getCartLines() as $key => $line) {
           $count = $count + $line->getQuantity() ;
        //    dump($line);
        }
        // dump($this->getCartLines()[2]->getQuantity());
        //a chaque tour, additione la line->quantity au total
        return $count;
        //return le total
    }
    
    public function getTotal()
    {
        $count = 0;

        foreach ($this->getCartLines() as $key => $cartLine) {
          $count =$count + ($cartLine->getArticle()->getPrice()*$cartLine->getQuantity());
        }

        return $count;
    }
}
