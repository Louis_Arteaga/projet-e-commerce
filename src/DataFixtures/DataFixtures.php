<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Article;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Cart;


class DataFixtures extends Fixture
{

    private const IMG = [
        'https://i5.walmartimages.com/asr/7b18a34e-6aef-41e3-ad44-0f6a52de060e_1.279c377a9b8f5875186829b8754d35d8.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF',
        'https://cdn02.plentymarkets.com/4in9ikd3ipve/item/images/810092/full/810092-28-Zoll-Galano-Prelude-Citybike-Stadt-Fah_5.jpg',
        'https://keyassets.timeincuk.net/inspirewp/live/wp-content/uploads/sites/2/2018/01/btwin-ultra-900-af-2017.jpg'

    ];
    private $encoder;


    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        
        $user1 = new User();
        $user1->setName('nidal');
        $user1->setEmail('nidal@nidal.com');
        $user1->setPassword($this->encoder->encodePassword($user1, '12345'));     
        $user1->setRole('ROLE_ADMIN');
        $user1->setCart(new Cart());        
        $manager->persist($user1);

        $user2 = new User();
        $user2->setName('louis');
        $user2->setEmail('louis@louis.com');
        $user2->setPassword($this->encoder->encodePassword($user2, '12345'));     
        $user2->setRole('ROLE_USER');
        $user2->setCart(new Cart());
        $manager->persist($user2);

        for ($i=1; $i <= 5; $i++) { 
            $article1 = new Article();
            $article1->setType('City Bike');
            $article1->setModel('Bike'.$i);
            $article1->setPrice(1200);
            // $article1->setImageURL('https://www.wikichat.fr/wp-content/uploads/sites/2/comment-soigner-une-plaie-dun-chat.jpg');
            $article1->setImageURL(self::IMG[1]);
            $manager->persist($article1);
        }

        for ($x=1; $x <= 5; $x++) { 
            $article2 = new Article();
            $article2->setType('Mountain Bike');
            $article2->setModel('Bike'.$x);
            $article2->setPrice(1200);
            //$article2->setImageURL('https://www.wikichat.fr/wp-content/uploads/sites/2/comment-soigner-une-plaie-dun-chat.jpg');
            $article2->setImageURL(self::IMG[0]);
            $manager->persist($article2);
        }

        for ($j=1; $j <= 5; $j++) { 
            $article3 = new Article();
            $article3->setType('Race Bike');
            $article3->setModel('Bike'.$j);
            $article3->setPrice(1200);
            //$article3->setImageURL('https://www.wikichat.fr/wp-content/uploads/sites/2/comment-soigner-une-plaie-dun-chat.jpg');
            $article3->setImageUrl(self::IMG[2]);
            $manager->persist($article3);
        }



        // $comment1 = new Comment();
        // $comment1->setTitle('Liked');
        // $comment1->setContent('I love this product.');
        // $comment1->setDate(new \DateTime('2018-03-21'));
        // $manager->persist($comment1);

        // $comment2 = new Comment();
        // $comment2->setTitle('Dis-Liked');
        // $comment2->setContent('I did not liked this product.');
        // $comment2->setDate(new \DateTime('2017-02-20'));
        // $manager->persist($comment2);

        // $comment3 = new Comment();
        // $comment3->setTitle('Dis Liked');
        // $comment3->setContent('I will recommend this product to others.');
        // $comment3->setDate(new \DateTime('2016-01-19'));
        // $manager->persist($comment3);

        
        $manager->flush();
    }
}
