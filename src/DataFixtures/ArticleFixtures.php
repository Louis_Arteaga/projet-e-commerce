<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Service\UploadService;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\Article;
use Faker;



class ArticleFixtures extends Fixture implements DependentFixtureInterface
{
    private $uploader;
    private $filesystem;
    private $faker;
    public const ARTICLE_REFERENCE = 'article';

   
    public function __construct(UploadService $uploader, Filesystem $filesystem)
    {
        $this->uploader = $uploader;
        $this->filesystem = $filesystem;
        $this->faker = Faker\Factory::create('en_EN');
    }

    public function load(ObjectManager $manager)
    {
        $this->filesystem->remove($_ENV['UPLOAD_DIRECTORY']);

        
        for ($y = 1; $y < 2; $y++) {
            
            // $user = $this->getReference(UserFixtures::USER_REFERENCE . $y);
            
            for ($x = 1; $x <= 4; $x++) {

                $article = new Article();
                $article->setType($this->faker->company);
                $article->setModel($this->faker->userName(8));
                $article->setPrice($this->faker->randomDigit(4));
                $imageURL = $this->uploader->upload(new File($this->faker->image()));
                $article->setImageURL($imageURL);
                $manager->persist($article);
            $this->addReference(self::ARTICLE_REFERENCE . $x, $article);

            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
