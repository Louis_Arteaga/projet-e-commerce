<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Comment;
use App\Service\UploadService;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker;


class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    private $faker;

    public function __construct(UploadService $uploader, Filesystem $filesystem)
    {
        $this->faker = Faker\Factory::create('en_EN');
    }

    public function load(ObjectManager $manager)
    {

        for ($z = 1; $z <= 4; $z++) {

            $article = $this->getReference(ArticleFixtures::ARTICLE_REFERENCE . $z);
            $user = $this->getReference(UserFixtures::USER_REFERENCE . $z);

            for ($x = 1; $x <= 6; $x++) {

                $comment = new Comment();
                $comment->setTitle($this->faker->randomLetter(16));
                $comment->setContent($this->faker->sentence(50));
                $comment->setDate(new \DateTime('2019-10-0' . $x));
                $comment->setArticle($article);
                $comment->setUser($user);
                $manager->persist($comment);
            }
        }

        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            ArticleFixtures::class
        ];
    }
}
