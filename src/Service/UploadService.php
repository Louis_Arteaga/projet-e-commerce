<?php

namespace  App\Service;


use Symfony\Component\HttpFoundation\File\File;


class UploadService {

    public function upload(File $file):string {
        $name = md5( uniqid()) . '.' . $file->guessExtension();

        $file->move($_ENV['UPLOAD_DIRECTORY'], $name);

        return $name;
    }
}
