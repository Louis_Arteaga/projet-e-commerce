<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Form\ArticleType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Repository\ArticleRepository;
use App\Form\CommentType;
use App\Entity\Comment;

class AdminController extends AbstractController
{


    /**
     * @Route("/admin/index", name="admin_index")
     */
    public function adminIndex()
    {

        return $this->render("admin/index.html.twig");
    }



    /**
     * @Route("/admin/add-article", name="add_article")
     */
    public function index(Request $request, ObjectManager $objectManager)
    {

        $article = new Article();
        //on fait notre formulaire classique
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $objectManager->persist($article);

            $objectManager->flush();
            dump($article);

            return $this->redirectToRoute("show_articles");
        }


        return $this->render('admin/add-form.html.twig', [
            'controller_name' => 'AdminController',
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/admin/show-articles", name="show_articles")
     */
    public function showArticles(ArticleRepository $repo)
    {

        return $this->render("article/index.html.twig", [
            "articleData" => $repo->findAll()

        ]);
    }

    /**
     * @Route ("/admin/one-article/{id}", name="show_one_article")
     */
    public function oneArticle(Article $article, Request $request, ObjectManager $objectManager)
    {
        $comment = new Comment();

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $comment->setDate(new \DateTime);

            $comment->setUser($this->getUser());
            $comment->setArticle($article);


            $objectManager->persist($comment);

            $objectManager->flush();
            // dump($comment);

        }

        return $this->render("admin/one-article.html.twig", [
            "articleList" => $article,
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/update/{id}", name="update")
     */
    public function Update(Request $request, ObjectManager $repo, Article $article)
    {

        $form = $this->createForm(ArticleType::class, $article);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $repo->flush();
            dump($article);
            return $this->redirectToRoute("show_articles");
        }
        $articleData = $form->getData();



        return $this->render("admin/add-form.html.twig", [
            'form' => $form->createView(),
            'articleData' => $articleData
        ]);
    }

    /**
     * @Route ("/admin/delete-article/{id}", name="delete_article")
     */
    public function Remove(ObjectManager $repo, Article $article)
    {

        $repo->remove($article);
        $repo->flush();

        return $this->redirectToRoute("show_articles");
    }

    /**
     * @Route("/admin/show-search", name="admin_search")
     */
    public function searchArticle(Request $request, ArticleRepository $repo)
    {
        dump($request->get('search'));
        $results = $repo->findByType($request->get('search'));

        return $this->render('admin/search-article.html.twig', [
            'resultList' => $results


        ]);
    }

    /**
     * @Route("/admin/delete/{comment} ", name="delete_comment")
     */
    public function delete(ObjectManager $manager, Comment $comment)
    {
        $user = $this->getUser();
        
        if ($user->getRole() === 'ROLE_ADMIN') {
            $article=$comment->getArticle();
            dump($article);
            $manager->remove($comment);
            $manager->flush();
            return $this->redirectToRoute("show_one_article", [
                "id" => $article->getId()
            ]);

        } else {
                return new response('You do not have permission to delete this comment.');
            }
    }
}
