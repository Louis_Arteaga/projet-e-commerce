<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Article;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Entity\User;
use App\Repository\ArticleRepository;
use App\Form\CommentType;
use App\Entity\Comment;



class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }



    /**
     * @Route("/profile/show-articles", name="profile_show_articles")
     */
    public function profile(ArticleRepository $repo)
    {

        $article = $repo->findAll();
        // $comments = $article->getComments();
        // dump($comments);die;

        return $this->render('article/index.html.twig', [
            'articleData' => $article
        ]);
    }

    /**
     * @Route ("/profile/one-article/{id}", name="profile_show_one_article")
     */
    public function oneArticle(Article $article, ObjectManager $objectManager, Request $request)
    {

        $comment = new Comment();

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $comment->setDate(new \DateTime);

            $comment->setUser($this->getUser());
            $comment->setArticle($article);


            $objectManager->persist($comment);

            $objectManager->flush();
            // dump($comment);

        }

            // return $this->redirectToRoute("profile_show-articles");
            return $this->render("user/one-article.html.twig", [
                "articleList" => $article,
                'form'=> $form->createView()
            ]);
        }

        /**
     * @Route("/login", name="login")
     */
    
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        //$lastUsername = $authenticationUtils->getLastUsername();

        
        return $this->render('user/login.html.twig', [
            'error' => $error,
            //'userName' => $lastUsername
        ]);
        return $this->redirectToRoute('profile_show_articles');
    }
    /**
     * @Route("/profile/update_user", name="update_user")
     */
    public function updateUser(Request $request, UserPasswordEncoderInterface $encoder)
    {

        $user = $this->getUser();

        if ($this->getUser() !== $user) {
            return new Response("You dont have permisson to change this credentials.", 401);
        }
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $pass = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($pass);

            $userData = $this->getDoctrine()->getManager();
            $userData->flush();

            return $this->redirectToRoute('login');
        }

        return $this->render("user/update-user.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /** 
     * @Route("/search-user", name="user_search")
     */
    public function searchArticle(Request $request, ArticleRepository $repo)
    {
        dump($request->get('search'));
        $results = $repo->findByType($request->get('search'));

        

        return $this->render('user/search-article.html.twig', [
            'resultList' => $results
        ]);
        return $this->redirectToReferer();
    }
}
