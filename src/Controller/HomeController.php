<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Comment;

use App\Form\CommentType;


class HomeController extends AbstractController
{
    /**
     * @Route("/", name="welcome_page")
     */
    public function ShowLandingPage()
    {
        return $this->render('home/home.html.twig');
    }
    /**
     * @Route("/welcome", name="home")
     */
    public function ShowHome(ArticleRepository $repo)
    {

        $article = $repo->findAll();
        
        return $this->render('home/index.html.twig', [
            'articleData' => $article
        ]);
        return $this->render('home/home.html.twig', []);
    }

    /**
     * @Route("/articles", name="articles")
     */
    public function ShowArticle(ArticleRepository $repo)
    {

        $article = $repo->findAll();

        return $this->render('article/index.html.twig', [
            'articleData' => $article
        ]);
    }

    /**
     * @Route("/role-redirection", name="role_redirection")
     */
    public function loginRedirection(ArticleRepository $repo)
    {
        $user = $this->getUser();
        if ($user->getRole() === 'ROLE_ADMIN') {

            return $this->redirectToRoute('admin_index');
        } else {

            return $this->redirectToRoute('profile_show_articles');
        }
        return $this->render('article/index.html.twig', [
            'articleData' => $repo->findAll()
        ]);
    }
    /**
     * @Route("/article/{id}", name="one_article")
     */
    public function oneArticle(Article $article, ObjectManager $objectManager,Request $request)
    {

        $comment = new Comment();

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $comment->setDate(new \DateTime);

            $comment->setUser($this->getUser());
            $comment->setArticle($article);


            $objectManager->persist($comment);

            $objectManager->flush();
            // dump($comment);

            // return $this->redirectToRoute("profile_show-articles");
        }
        return $this->render('article/one-article.html.twig', [
            'articleData' => $article,
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route("show-search", name="search")
     */
    public function searchArticle(Request $request, ArticleRepository $repo)
    {
        dump($request->get('search'));
        $results = $repo->findByType($request->get('search'));

        return $this->render('home/search-article.html.twig', [
            'resultList' => $results
        ]);
    }
    
}
