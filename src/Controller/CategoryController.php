<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;

class CategoryController extends AbstractController
{


    /**
     * @Route("/category/city-bike", name="city_bike")
     */
    public function cityBike(ArticleRepository $repo)
    {
        return $this->render('category/city-bike.html.twig', [
            'bikes' => $repo->findBy(['type' => 'City Bike'])
        ]);
    }

    /**
     * @Route("/category/mountain-bike", name="mountain_bike")
     */
    public function mountainBike(ArticleRepository $repo)
    {
        return $this->render('category/mountain-bike.html.twig', [
            'bikes' => $repo->findBy(['type' => 'Mountain Bike'])
        ]);
    }

        /**
     * @Route("/category/race-bike", name="race_bike")
     */
    public function raceBike(ArticleRepository $repo)
    {
        return $this->render('category/race-bike.html.twig', [
            'bikes' => $repo->findBy(['type' => 'Race Bike'])
        ]);
    }
}
