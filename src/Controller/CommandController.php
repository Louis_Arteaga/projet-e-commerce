<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Command;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Cart;
use App\Entity\Article;
use App\Entity\CartLine;
use App\Repository\CartRepository;
use Symfony\Component\HttpFoundation\Request;

class CommandController extends AbstractController
{
    /**
     * @Route("/command", name="command")
     */
    public function index()
    {
        return $this->render('command/index.html.twig', [
            'controller_name' => 'CommandController',
        ]);
    }


    /**
     * @Route("/add-to-cart/{article}", name="add_to_cart")
     */
    public function addCart(ObjectManager $objectManager, Article $article, Request $request)
    {


        $quantity = $request->get("quantity");

        if (!$this->getUser()->getCart()) {
            $cart = new Cart();
            $this->getUser()->setCart($cart);
            $objectManager->persist($cart);
        } else {
            $cart = $this->getUser()->getCart();
        }
        
        $cartline = new CartLine();
        $cartline->setArticle($article);
        $cartline->setQuantity($quantity);

        $objectManager->persist($cartline);
        $cart->addCartline($cartline);

        $objectManager->persist($cart);

        $objectManager->flush();
        dump($cart);

        return $this->redirectToRoute('one_article', ['id' => $article->getId()]);
    }

    /**
     * @Route("/commande/{cart}", name="command")
     */
    public function command(ObjectManager $objectManager, Cart $cart)
    {

        $cart = $this->getUser()->getCart();
        $cart->getCartLines();

        $command = new Command();
        $this->getUser()->setCart($cart);
        $objectManager->persist($cart);
        
        $command->setUser($this->getUser());
        $command->setDate(new \DateTime());
        foreach ($cart->getCartLines() as $key => $cartLine) {

            $command->addCartLine($cartLine);
            $cart->removeCartLine($cartLine);
        }



        $objectManager->persist($command);
        $objectManager->flush();


        return $this->redirectToRoute('command-validation');
    }

    /**
     * @Route("/profile/my-cart", name = "my_cart")
     */
    public function myCart(Request $request)
    {


        return $this->render('command/my-cart.html.twig', [
            'cart' => $this->getUser()->getCart(),
        ]);

        return $this->redirectToRoute('my_cart');
    }

    /**
     * @Route("/profile/command-validation", name = "command-validation")
     */
    public function commandValidation()
    {
        return $this->render('command/command-validation.html.twig');
    }

    /**
     * @Route ("/profile/delete-article/{id}", name="profile_delete_article")
     */
    public function Remove(ObjectManager $manager, CartLine $cartLine)
    {

        $manager->remove($cartLine);
        $manager->flush();

        return $this->redirectToRoute("my_cart");
    }

    /**
     * @Route("/profile/modify-quantity/{id}",name="profile_modify_quantity")
     */
    public function modifyQuantity(ObjectManager $manager, CartLine $cartLine, Request $request)
    {
        $quantity = $request->get('quantity');
        $cartLine->setQuantity($quantity);
        $manager->flush();

        return $this->redirectToRoute("my_cart");
    }
}
