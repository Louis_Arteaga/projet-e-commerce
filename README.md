# Vélo'Commerce

This project was created at simplon to increase our's knowledge of web development skills. Also to develope our ability to work in a group using GIT.

## Use Case

<img src="./public/images/usecase.png" alt="use-case">

## Challanges Overcame

Working in autonmy yet as in group, we have to come up with the solution to understand each others point of view.
To soothen this process we have used agile framework where we did; 
- daily scrums
- weekly sprints
- Weekly speedboat


## Future modification

The design can be improvised in the future.

## Images of the website

<img src="./public/images/landing-page.png" alt="landing-page">
<img src="./public/images/register-2.png" alt="register">
<img src="./public/images/login2.png" alt="login">
<img src="./public/images/user-page.png" alt="user-page">
<img src="./public/images/category.png" alt="category">
<img src="./public/images/cart.png" alt="cart">
<img src="./public/images/comments.png" alt="comments">
<img src="./public/images/error-search.png" alt="error-search">




## Built With

* [Symfony4](https://symfony.com/4) - The back end PHP framework we used.
* [Twig](https://twig.symfony.com/) - The templating language we used.
* [Doctrine](https://fr.wikipedia.org/wiki/Doctrine_(ORM)) - The ORM we used.
* [SQL](https://fr.wikipedia.org/wiki/Structured_Query_Language) - The Query language we used.
* [Bootstrap](https://getbootstrap.com/) - The CSS framework we used.



## Authors

* [Kalpesh Ghaag](https://gitlab.com/kalpesh.ghaag)
* [Nidal Zaiani](https://gitlab.com/Nidal.Simplon)
* [Louis Arteaga](https://gitlab.com/Louis_Arteaga)

## Version 

This project is currently at the first release 1.0.0

## License

This project is licensed under the MIT License 

## Special Thanks

* Jean and Pierre for helping us.
* Other groups.


